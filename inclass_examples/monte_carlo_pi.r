#pt <- runif(2, min=-1, max=1)
#print(pt)
#print(sum(pt^2))

accept <- function(p) {
  sum(p^2) <= 1
}

N <- 5000000
accepted <- 0

s <- proc.time()
for (i in 1:N) {
  pt <- runif(2, min=0, max=1)
  if (accept(pt))
    accepted <- accepted + 1
}
print(proc.time() - s)
#print(accepted)
#print(accepted / N * 4)

s <- proc.time()
acc <- replicate(N, accept(runif(2)))
print(proc.time() - s)
print(sum(acc) / N * 4)