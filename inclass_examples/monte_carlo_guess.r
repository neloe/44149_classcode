N <- 1000
X1BOUNDS <- c(0, 1)
X2BOUNDS <- c(0, 2)
X3BOUNDS <- c(2, 3)

#maximize z on the following domain:
# 0 <= x1 <= 1
# 0 <= x2 <= 2
# 2 <= x3 <= 3
z <- function(x1, x2, x3) {
  (exp(x1) + x2)^2 + 3*(1-x3)^2
}

generate_guess <- function () {
  X1 <- runif(1, min=0, max=1)
  X2 <- runif(1, min=0, max=2)
  X3 <- runif(1, min=2, max=3)
  
  val <- z(X1, X2, X3)
  c(X1, X2, X3, val)
}

#maxval <- 0
#maxparam <- c(0, 0, 0)
maximum <- c(0, 0, 0, 0)

for (i in 1:N) {
  guess <- generate_guess()
  if (guess[4] > maximum[4])
    maximum <- guess
  
  
  #if (val > maxval) {
  #  maxval <- val
  #  maxparam <- c(X1, X2, X3)
  #}
}

print(maximum)
#print('Max value:')
#print(maxval)
#print('Coordinates:')
#print(maxparam)
