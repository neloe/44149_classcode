K <- 2
M <- 1
V0 <- 0
X0 <- 2
ITERS <- 100
TMAX <- 10

a <- function(x, dt){
  - K * x / M * dt
}

t <- seq(0, TMAX, length=ITERS)
x <- rep(0, ITERS)
v <- rep(0, ITERS)
x[1] <- X0
v[1] <- V0

for (i in 2:ITERS) {
  deltat <- (t[i] - t[i-1])
  v[i] <- v[i-1] + a(x[i-1], deltat)
  x[i] <- x[i-1] + v[i] * deltat
}

#plot(t, x, ylim=c(-3,3))
#points(t, v, col='red')
if (!require(plotly)) {
  install.packages("plotly")
  require(plotly)
}

accumulate_t = c(t[1])
accumulate_x = c(x[1])
frames = c(1)

for (frame in 2:ITERS) {
  accumulate_t = c(accumulate_t, t[1:frame])
  accumulate_x = c(accumulate_x, x[1:frame])
  frames = c(frames, rep(t[frame], frame))
}

p <- plot_ly(x=accumulate_t, y=accumulate_x, frame=frames, type='scattergl', mode='markers+lines')
p <- animation_opts(p, 1, redraw=FALSE)
print(p)
#link = api_create(p, 'spring_inclass')
#print(link)
