# print(sample(1:50, 5, replace=FALSE))

A <- 1664525
C <- 2^32
M <- 1013904223

lcg <- function(r) {
  (A * r + C) %% M
}

rn <- 1

for (i in 1:10) {
  rn <- lcg(rn)
  print(rn)
}