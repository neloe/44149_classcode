K <- 2
M <- 1
V0 <- 0
X0 <- 2
STEPS <- 100
TMAX <- 10
G <- 9.8
Fn <- G*M
MUk <- .1
a <- function(x){
  spring <- - K * x / M
  # friction force opposes
  if (spring < 0)
    spring <- spring + MUk * Fn
  else
    spring <- spring - MUk * Fn
  spring
}

t <- seq(0, TMAX, length=STEPS)
x <- rep(0, STEPS)
v <- rep(0, STEPS)
x[1] <- X0
v[1] <- V0

for (i in 2:STEPS) {
  dt <- (t[i] - t[i-1])
  v[i] <- v[i-1] + a(x[i-1]) * dt
  x[i] <- x[i-1] + v[i] * dt
  
}

plot(t, x, ylim=c(-3,3))
points(t, v, col='red')

