scollatz_len <- function(n)
{
  count <- 0
  while (n > 1)
  {
    if (n %% 2 == 0) n <- n/2
    else n <- 3 * n + 1
    count <- count + 1
  }
  count
}

collatz_len <- Vectorize(scollatz_len)
lens <- collatz_len(1:1000)
plot(lens)