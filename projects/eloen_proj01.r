# Name: Nathan Eloe
# Course: 44-149 Scientific Computing
# Project 01
# Due Date: 
# Brief: 3D chaotic attractors
# By submitting this, I pledge that the code in this file was written by the author indicated above,
#  and that all assistance from outside sources was correctly attributed in comments.  Additionally, I 
#  agree to abide by the rules expressed in the CSIS Academic Honesty Policy

SIGMA <- 10
RHO <- 28
BETA <- 8.0/3
ITERS <- 10000
TMAX <- 100
X0 <- 1
Y0 <- 1
Z0 <- 1

dx <- function(x, y, z, dt){
  SIGMA * (y - x) * dt
}

dy <- function(x, y, z, dt) {
  (x * (RHO - z) - y) * dt
}

dz <- function(x, y, z, dt) {
  (x * y - BETA * z) * dt
}
t <- seq(0, TMAX, length=ITERS)
x <- rep(0, ITERS)
y <- rep(0, ITERS)
z <- rep(0, ITERS)

x[1] <- X0
y[1] <- Y0
z[1] <- Z0

for (i in 2:ITERS) {
  deltat <- t[i] - t[i-1]
  x[i] = x[i-1] + dx(x[i-1], y[i-1], z[i-1], deltat)
  y[i] = y[i-1] + dy(x[i-1], y[i-1], z[i-1], deltat)
  z[i] = z[i-1] + dz(x[i-1], y[i-1], z[i-1], deltat)
}

if (!require(plotly)) {
  install.packages('plotly')
  require(plotly)
}

accx = x[1]
accy = y[1]
accz = z[1]
frames = t[1]

#for (frame in 2:ITERS) {
#  accx <- c(accx, x[1:frame])
#  accy <- c(accy, y[1:frame])
#  accz <- c(accz, z[1:frame])
#  frames <- c(frames, rep(t[frame], frame))
#}
#p <- plot_ly(x=accx,y=accy,z=accz, frame=frames, type='scatter3d', mode='lines', line = list(simplyfy = F))
p <- plot_ly(x=x,y=y,z=z,type='scatter3d', mode='lines', line = list(simplyfy = F))
#animation_opts(p, 1, redraw=F)
#layout(p, xaxis=)
print(p)