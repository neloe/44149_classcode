# Name: Nathan Eloe
# Course: 44-149 Scientific Computing
# Lab 03
# Due Date: 
# Brief: Calculating the length of the longest collatz chain
# By submitting this, I pledge that the code in this file was written by the author indicated above,
#  and that all assistance from outside sources was correctly attributed in comments.  Additionally, I 
#  agree to abide by the rules expressed in the CSIS Academic Honesty Policy

MAX_START <- 1000000

collatz_chain_length <- function(n) {
  length <- 0
  while (n > 1) {
    if (n %% 2 == 0)
      n <- n / 2
    else
      n <- 3 * n + 1
    length <- length + 1
  }
  length
}

longest_chain <- 0
start <- 1

for (i in 2:MAX_START) {
  len <- collatz_chain_length(i)
  if (len > longest_chain) {
    longest_chain <- len
    start <- i
  }
}

print(collatz_chain_length(13) == 9)
print(collatz_chain_length(64) == 6)
print(collatz_chain_length(21) == 7)
print(collatz_chain_length(100) == 25)

print(start)

