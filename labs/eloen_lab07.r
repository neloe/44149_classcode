# Name: Nathan Eloe
# Course: 44-149 Scientific Computing
# Lab 07
# Due Date: 
# Brief: 3D transformations using matrix math
# By submitting this, I pledge that the code in this file was written by the author indicated above,
#  and that all assistance from outside sources was correctly attributed in comments.  Additionally, I 
#  agree to abide by the rules expressed in the CSIS Academic Honesty Policy

if(!require(plotly))
{
  install.packages("plotly")
  require(plotly)
}
translate <- function (x, y, z) {
  tr <- diag(rep(1,4))
  tr[,4] <- c(x, y, z, 1)
  tr
}

project <- function(l, r, b, t, n, f) {
  mat <- diag(c((2 * n)/ (r - l), (2 * n)/(t - b), -(f+n)/(f-n), 0))
  mat[1, 3] <- (r+l)/(r-l)
  mat[2, 3] <- (t+b)/(t-b)
  mat[4, 3] <- -1
  mat[3, 4] <- (-2*f*n)/(f-n)
  mat
}

scale <- function (x, y, z) {
  diag(c(x, y, z, 1))
}

p11 <- c(1,1,3, 1)
p12 <- c(1,-1,3,1)
p13 <- c(-1,-1,3,1)
p14 <- c(-1,1,3,1)

p21 <- c(1,1,5,1)
p22 <- c(1,-1,5,1)
p23 <- c(-1,-1,5,1)
p24 <- c(-1,1,5,1)

m <- matrix(c(p11, p21, p12, p11, p14, p24, p21, p14, p13, p23, p14,
              p12, p13, p22, p23, p14, p24, p23, p21, p22, p12), nrow=4)
m <- as.matrix(read.csv('points.csv', header=TRUE, row.names=1))
p <- plot_ly(x=m[1,] / m[4,], y=m[2,] / m[4,], z=m[3,] / m[4,], type='scatter3d', mode='lines+markers')


m1 <- scale(2, 2, 2) %*% m
T1 <- translate(0, 0, -4)
S <- scale(1.5, 1.5, 1.5)
T2 <- translate(0, 0, 4)
m2 <- T2 %*% S %*% T1 %*% m
m3 <- project(-2, 2, -2, 2, 2, 20) %*% m
m3[3, ] <- 0
# ----

allm <- cbind(m, m1, m2, m3)
labels <- c(rep('original', ncol(m)), rep('scaled', ncol(m)), rep('scaled in place', ncol(m)), rep('projected', ncol(m)))
p <- plot_ly( x=allm[1,] / allm[4,], y=allm[2,] / allm[4,], z=allm[3,] / allm[4,], type='scatter3d', mode='lines+markers', color=labels)

print(p)