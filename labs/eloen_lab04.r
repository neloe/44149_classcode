# Name: Nathan Eloe
# Course: 44-149 Scientific Computing
# Lab 04
# Due Date: 
# Brief: Plotting cardiods and hypocycloids
# By submitting this, I pledge that the code in this file was written by the author indicated above,
#  and that all assistance from outside sources was correctly attributed in comments.  Additionally, I 
#  agree to abide by the rules expressed in the CSIS Academic Honesty Policy

t <- seq(0, 20, len=1000)
a <- .5
b <- 1.0
x <- a * cos(2*pi*t)*(1 - cos(2*pi*t))
y <- b * sin(2*pi*t)*(1 - cos(2*pi*t))

plot(x, y, type="l", xlim=c(-2, 2))

# Bonus credit for one hypocycloid

x <- (a-b)*cos(2*pi*t)-b*cos((a-b)/b*2*pi*t)
y <- (a-b)*sin(2*pi*t)-b*sin((a-b)/b*2*pi*t)

lines(x, y, col='red')