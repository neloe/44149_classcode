# Name: Nathan Eloe
# Course: 44-149 Scientific Computing
# Lab 02
# Due Date: 
# Brief: Sum fibbonaci numbers
# By submitting this, I pledge that the code in this file was written by the author indicated above,
#  and that all assistance from outside sources was correctly attributed in comments.  Additionally, I 
#  agree to abide by the rules expressed in the CSIS Academic Honesty Policy

fib1 <- 1
fib2 <- 2
total <- 1

MAXFIB <- 4000000

while (fib2 < MAXFIB) {
  total <- total + fib2
  c <- fib1 + fib2
  fib1 <- fib2
  fib2 <- c
}

print("Result")
print(total)

# Extra Credit

a <- 14
b <- 21

print("A:")
print(a)
print("B:")
print(b)

while (b != 0) {
  t <- b
  b <- a %% b
  a <- t
}

print("GCD of A and B:")
print(a)
