# Name: Nathan Eloe
# Course: 44-149 Scientific Computing
# Lab 06
# Due Date: 
# Brief: Investigating the error in differentiation
# By submitting this, I pledge that the code in this file was written by the author indicated above,
#  and that all assistance from outside sources was correctly attributed in comments.  Additionally, I 
#  agree to abide by the rules expressed in the CSIS Academic Honesty Policy

xval <- 3

f <- function(x) {
  x^3 + 2 * x - 3
}

fprime <- function(x) {
  3 * x^2 + 2
}

deriv <- function(x, h) {
  (f(x+h)-f(x)) / h
}

err <- function(h) {
  abs((fprime(xval) - deriv(xval, h))/fprime(xval))
}

hvals <- seq(.00001, 1, length=100)
errs <- err(hvals)

plot(hvals, errs, type='l')
