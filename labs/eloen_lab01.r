# Name: Nathan Eloe
# Course: 44-149 Scientific Computing
# Lab 01
# Due Date: 
# Brief: Convert temperatures from F to C and back to F
# By submitting this, I pledge that the code in this file was written by the author indicated above,
#  and that all assistance from outside sources was correctly attributed in comments.  Additionally, I 
#  agree to abide by the rules expressed in the CSIS Academic Honesty Policy

temp_fahr <- 212

# uncomment this for the extra credit
# temp_fahr <- as.numeric(readline("Enter a temperature in fahrenheit: "))

print("Original Temp:")
print(temp_fahr)

temp_celc <- (temp_fahr - 32) * 5 / 9

print("converted Temp:")
print(temp_celc)

new_temp_fahr <- (temp_celc * 9 / 5) + 32
print("Reconverted Temp:")
print(new_temp_fahr)

if (temp_celc < 0)
  print("It is below freezing")
if (temp_celc > 100)
  print("HOT HOT HOT!")