# Name: Nathan Eloe
# Course: 44-149 Scientific Computing
# Lab 05
# Due Date: 
# Brief: Reading csvs and plotting the results
# By submitting this, I pledge that the code in this file was written by the author indicated above,
#  and that all assistance from outside sources was correctly attributed in comments.  Additionally, I 
#  agree to abide by the rules expressed in the CSIS Academic Honesty Policy

tab <- read.csv('input.csv')

#required part
plot(tab[2:3])

# one way to do the 3d part
# install.package("plot3D")
#library("plot3D")
#lines3D(tab[,'x'], tab[,'y'], tab[,'z'])